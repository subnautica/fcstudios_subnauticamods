﻿using System;
using System.Collections;
using TMPro;
using Unity.Collections;
using UnityEngine;

public class PistonController : MonoBehaviour
{
    private float _percentage;
    [SerializeField]
    private float _countDownSeconds = 2;
    [SerializeField]
    [ReadOnly]
    private float _timer;
    private float _timeMultiplier = 0f;

    // Position Storage Variable
    private Vector3 originalLocalPosition;
    [SerializeField]
    private bool _isRunning;

    private bool _isReleasing;
    public Action OnPressureReleased { get; set; }

    // Use this for initialization
    private void Start()
    {
        // Store the starting position of the object relative to its parent.
        originalLocalPosition = transform.localPosition;
        _timeMultiplier = 1 / _countDownSeconds;
    }

    public void SetState(bool isRunning)
    {
        _isRunning = isRunning;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!_isRunning) return;

        if (_isReleasing)
        {
            _percentage -= Time.deltaTime * _timeMultiplier; //Count
            _timer += Time.deltaTime; // Just show the time passed from the beginning 

            // Calculate offset
            // Even though Transform.up is relative, it is in world space
            // That means that you can't apply it to localPosition
            //
            // Vector3.up could be used along with localPosition, but that would ignore the object's rotation

            Vector3 globalOffset = transform.up * _percentage;

            // Reset the position to the original
            transform.localPosition = originalLocalPosition;

            // Apply offset
            transform.position += globalOffset;

            if (!(_percentage <= 0)) return;
            OnPressureReleased?.Invoke();
            _isReleasing = false;
        }
        else
        {
            // Calculate offset
            // Even though Transform.up is relative, it is in world space
            // That means that you can't apply it to localPosition
            //
            // Vector3.up could be used along with localPosition, but that would ignore the object's rotation

            Vector3 globalOffset = transform.up * _percentage;

            // Reset the position to the original
            transform.localPosition = originalLocalPosition;

            // Apply offset
            transform.position += globalOffset;
        }

        
    }

    public void SetPercentage(float amount)
    {
        _percentage = amount;
        if (amount >= 100)
        {
            StartCoroutine(ReleasePressure());
        }
    }

    private IEnumerator ReleasePressure()
    {
        _isRunning = false;
        yield return new WaitForSeconds(2);
        _isRunning = true;
        _isReleasing = true;
        yield break;
    }
}
