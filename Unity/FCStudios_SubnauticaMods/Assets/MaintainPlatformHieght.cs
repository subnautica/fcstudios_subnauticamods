﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaintainPlatformHieght : MonoBehaviour
{
    public Transform Platform;
    private Transform _trans;
    public float offset = -0.98f;
    private const float _poleHeight = 1.5983f;

    private void Awake()
    {
        _trans = gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        var pos = Platform.localPosition.y;
        _trans.localScale = new Vector3(1f, pos < 1f ? 1 : (pos / _poleHeight) - offset, 1f);
    }
}
