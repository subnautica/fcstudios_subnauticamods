﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadialMenu : MonoBehaviour
{
    [SerializeField] 
    private GameObject EntryPrefab;
    
    private List<RadialMenuEntry> Entries;
    
    [SerializeField] 
    private float Radius = 300f;

    [SerializeField]
    private Sprite[] Icons;

    public int TabAmount = 8;

    void Update()
    {
        //Vector3 screenPoint = Camera.main.WorldToScreenPoint(Selectable.position);
        //Vector3 v = Input.mousePosition - screenPoint;
        //float angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        //Selectable.transform.rotation = Quaternion.AngleAxis(angle,Vector3.forward);
    }

    void Start()
    {
        Entries = new List<RadialMenuEntry>();
        Open();
    }

    void AddEntry(Sprite pIcon)
    {
        GameObject entry = Instantiate(EntryPrefab, transform);
        entry.transform.localScale = new Vector3(1.4f, 1.4f, 1.4f);
        RadialMenuEntry rme = entry.GetComponent<RadialMenuEntry>();
        rme.Initialize(transform.parent.Find("PageName").gameObject.GetComponent<Text>(),pIcon);
        Entries.Add(rme);
    }

    public void Open()
    {
        for (int i = 0; i < TabAmount; i++)
        {
            AddEntry(Icons[i]);
        }

        Rearrange();
    }

    void Rearrange()
    {
        var radiansOfSeperation = (Mathf.PI * 2) / Entries.Count;
        for (int i = 0; i < Entries.Count; i++)
        {
            var x = Mathf.Sin(radiansOfSeperation * i) * Radius;
            var y = Mathf.Cos(radiansOfSeperation * i) * Radius;

            Entries[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(x, y);
        }
    }
}
