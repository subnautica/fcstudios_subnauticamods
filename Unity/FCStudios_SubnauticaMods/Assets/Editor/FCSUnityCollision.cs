﻿using System;
using System.Text;
using UnityEditor;
using UnityEngine;

public class FCSUnityCollision : EditorWindow
{
    GameObject m_GameObject = null;
    private bool m_SelectionOnlyMode = false;
    private bool m_ColliderAddedFlag;
    private string _tag;
    private const string BoxColliderTag = "_fcsUnityBC";
    private const string BoundsTag = "_fcsUnityBounds";
    private const string MeshColliderTag = "_fcsUnityMC";
    private static StringBuilder _sb;

    [MenuItem("Window/FCS Unity Collision")]
    public static void ShowWindow()
    {
        var window = GetWindow<FCSUnityCollision>("FCS Unity Collision");
        window.minSize = new Vector2(362, 160);
        window.maxSize = new Vector2(362, 160);
        if (_sb == null)
        {
            _sb = new StringBuilder();
        }

    }

    /// <summary>
    /// Puts the string into the Clipboard.
    /// </summary>
    public static void CopyToClipboard(string str)
    {
        GUIUtility.systemCopyBuffer = str;
    }

    void OnGUI()
    {
        //Window Code

        var style = GUI.skin.GetStyle("label");
        style.wordWrap = true;
        style.fontStyle = FontStyle.Bold;

        GUILayout.BeginVertical();

        GUILayout.Label("Please select the GameObject (Selection Only Mode) or place the GameObject in the field below that you would like to process. Ensure all name tags from Cinema4D on the objects remain for successful operation.", style);

        GUILayout.Space(10);

        EditorStyles.label.wordWrap = true;

        m_GameObject = (GameObject)EditorGUILayout.ObjectField(m_GameObject, typeof(GameObject), true);

        GUILayout.Space(10);

        m_SelectionOnlyMode = EditorGUILayout.Toggle("Selection Only Mode", m_SelectionOnlyMode);

        GUILayout.Space(10);

        //Create a button to begin the process
        if (GUILayout.Button("Create Collider"))
        {
            Debug.Log("Converting meshes to box collider");

            if (m_SelectionOnlyMode)
            {
                ConvertSelectionToCollider();
            }
            else
            {
                if (m_GameObject == null)
                {
                    Debug.LogError("Please provide a GameObject to process.");

                    return;
                }

                ConvertToCollider(m_GameObject);
            }
        }

        //Create a button to begin the process
        if (GUILayout.Button("Create Collider and Bounds"))
        {
            Debug.Log("Converting meshes to box collider");

            if (m_SelectionOnlyMode)
            {
                ConvertSelectionToCollider();
                var go = Selection.activeGameObject;
                ConvertToBounds(go);
            }
            else
            {
                if (m_GameObject == null)
                {
                    Debug.LogError("Please provide a GameObject to process.");

                    return;
                }

                ConvertToCollider(m_GameObject);
            }
        }


        //Create a button to begin the process
        if (GUILayout.Button("Get Collider Bounds"))
        {
            Debug.Log("Getting Collider Bounds");

            ConvertSelectionToColliderString();
        }
        GUILayout.EndVertical();
    }

    /// <summary>
    /// Gets all the selected GameObjects and converts objects with the tag to a box collider.
    /// </summary>
    private void ConvertSelectionToCollider()
    {
        //Lets make sure there is something selected.
        if (Selection.gameObjects.Length < 1)
        {
            Debug.LogError("Your in \"Selection Only Mode\". Please select a GameObject to process.");
            return;
        }

        //For each selection let process the objects.
        foreach (GameObject gameObject in Selection.gameObjects)
        {
            ConvertToCollider(gameObject);
        }
    }

    /// <summary>
    /// Gets all the selected GameObjects and converts objects with the tag to a box collider.
    /// </summary>
    private void ConvertSelectionToColliderString()
    {

        if (_sb == null)
        {
            _sb = new StringBuilder();
        }

        //Lets make sure there is something selected.
        if (Selection.gameObjects.Length < 1)
        {
            Debug.LogError("Your in \"Selection Only Mode\". Please select a GameObject to process.");
            return;
        }

        var bc = Selection.activeGameObject.GetComponent<BoxCollider>();

        //var mesh = go.transform.Find("mesh");
        //if (mesh != null)
        //{
        //    var collider = mesh.gameObject.AddComponent<BoxCollider>();

        //    var parent = go.AddComponent<BoxCollider>();
        //    parent.center = collider.center;
        //    parent.size = collider.size;
        //    Destroy(collider);
        //}

        if (bc != null)
        {
            _sb.Clear();
            _sb.Append($"var center = new Vector3({bc.center.x}f, {bc.center.y}f, {bc.center.z}f);");
            _sb.Append(Environment.NewLine);
            _sb.Append($"var size = new Vector3({bc.size.x}f, {bc.size.y}f, {bc.size.z}f);");
            Debug.Log(_sb.ToString());
            CopyToClipboard(_sb.ToString());
            //bc.hideFlags |= HideFlags.HideInInspector;
            //DestroyImmediate(go);
        }
    }
    
    /// <summary>
    /// Convert all GameObjects with the tag to a box collider.
    /// </summary>
    /// <param name="go"></param>
    private void ConvertToBounds(GameObject go)
    {
        try
        {
            var renders = go.GetComponentsInChildren<MeshRenderer>(true);

            foreach (MeshRenderer mesh in renders)
            {
                //Lets store the current gameObject for use
                var currentGameObject = mesh.gameObject;

                //Try to add the collider to the object.

                if (currentGameObject.name.Contains(BoundsTag))
                {
                    AddCollider(currentGameObject, ColliderType.BoxCollider);
                    var collider = currentGameObject.GetComponent<BoxCollider>();
                    var parent = go.AddComponent<BoxCollider>();
                    parent.center = collider.center;
                    parent.size = collider.size;
                    Destroy(currentGameObject);
                    break;
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
            throw;
        }
    }
    
    /// <summary>
    /// Convert all GameObjects with the tag to a box collider.
    /// </summary>
    /// <param name="go"></param>
    private void ConvertToCollider(GameObject go)
    {
        try
        {
            var renders = go.GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer mesh in renders)
            {
                //Lets store the current gameObject for use
                var currentGameObject = mesh.gameObject;

                //Try to add the collider to the object.

                if (mesh.gameObject.name.Contains(BoxColliderTag))
                {
                    AddCollider(currentGameObject, ColliderType.BoxCollider);
                }

                if (mesh.gameObject.name.Contains(BoundsTag))
                {
                    AddCollider(currentGameObject, ColliderType.BoxCollider);
                }

                if (mesh.gameObject.name.Contains(MeshColliderTag))
                {
                    AddCollider(currentGameObject, ColliderType.MeshCollider);
                }

                //If the operation was successful remove the tag and rename;
                if (m_ColliderAddedFlag)
                {
                    var meshFilter = currentGameObject.GetComponent<MeshFilter>();
                    DeleteComponent(currentGameObject, meshFilter);
                    DeleteComponent(currentGameObject, mesh);
                    RemoveAndRenameTag(currentGameObject);
                    m_ColliderAddedFlag = false;
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
            throw;
        }
    }

    /// <summary>
    /// Removes the generated name tag from the GameObject and renames the object to the custom name.
    /// </summary>
    /// <param name="go"></param>
    private void RemoveAndRenameTag(GameObject go)
    {
        //Lets find the name tag
        var tagFlag = FindTag(go);

        if (tagFlag)
        {
            var currentName = go.name;
            go.name = currentName.Replace(_tag, string.Empty);
            Debug.Log(string.Format("Renamed {0} to {1}", currentName, go.name));
        }
    }

    /// <summary>
    /// Finds the tag on the provided GameObject
    /// </summary>
    /// <param name="go">GameObject to process</param>
    /// <param name="tag">The tag the is found returns <see cref="string"/></param>
    /// <returns>The <see cref="string"/> tag that was found.</returns>
    private bool FindTag(GameObject go)
    {
        _tag = string.Empty;

        if (go.name.Contains(BoxColliderTag))
        {
            _tag = BoxColliderTag;
            return true;
        }

        if (go.name.Contains(MeshColliderTag))
        {
            _tag = MeshColliderTag;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Deletes a component from the GameObject
    /// </summary>
    /// <param name="go">The GameObject to process</param>
    /// <param name="component">The component to delete</param>
    private void DeleteComponent(GameObject go, Component component)
    {
        //Make sure the GameObject isn't null
        if (go != null)
        {
            //Lets hide the component in the inspector before deleting
            component.hideFlags |= HideFlags.HideInInspector;
            //Destroy the component using DestroyImmediate because you cant use Destroy() during Edit Time
            DestroyImmediate(component);
        }
        else
        {
            //Debug.Log($"The component \"{component.name}\"  was not found on the \"{go.name}\" object");
            Debug.LogError("The GameObject was not found.");
        }
    }

    /// <summary>
    /// Adds a collider of a specific type to the GameObject
    /// </summary>
    /// <param name="go">The GameObject to process</param>
    /// <param name="collider">The collider type to add to the GameObject</param>
    private void AddCollider(GameObject go, ColliderType collider)
    {
        if (go == null) return;

        switch (collider)
        {
            case ColliderType.BoxCollider:

                //Lets check to see if there is a box collider on this gameObject.
                var bc = go.gameObject.GetComponent<BoxCollider>();

                //If there isn't any Box Collider lets add one.
                if (bc == null)
                {
                    go.gameObject.AddComponent<BoxCollider>();
                }

                Debug.Log(string.Format("Added Box Collider to {0}", go.name));
                m_ColliderAddedFlag = true;
                break;

            case ColliderType.MeshCollider:

                //Lets check to see if there is a mesh collider on this gameObject.
                var mc = go.gameObject.GetComponent<MeshCollider>();

                //If there isn't any Box Collider lets add one.
                if (mc == null)
                {
                    go.gameObject.AddComponent<MeshCollider>();
                }

                Debug.Log(string.Format("Added Mesh Collider to {0}", go.name));
                m_ColliderAddedFlag = true;
                break;

            default:
                m_ColliderAddedFlag = false;
                throw new ArgumentOutOfRangeException(collider.ToString(), collider, null);
        }
    }

    /// <summary>
    /// The types of collider allowed on the GameObjects
    /// </summary>
    enum ColliderType
    {
        BoxCollider,
        MeshCollider
    }
}
