﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreConsumerController : MonoBehaviour
{
    [Range(0,100)]
    public float percentage;
    private PistonController[] _pistons;
    private bool _waitUntilPressureReleased;
    private HashSet<ParticleSystem> _smokeParticleEmitters = new HashSet<ParticleSystem>();
    private HashSet<ParticleSystem> _bubblesParticleEmitters = new HashSet<ParticleSystem>();
    [SerializeField]
    private bool _isUnderWater;

    // Start is called before the first frame update
    void Start()
    {
        _smokeParticleEmitters.Add(gameObject.transform.Find("WhiteSmoke").GetComponent<ParticleSystem>());
        _smokeParticleEmitters.Add(gameObject.transform.Find("WhiteSmoke_1").GetComponent<ParticleSystem>());
        _bubblesParticleEmitters.Add(gameObject.transform.Find("xBubbles").GetComponent<ParticleSystem>());
        _bubblesParticleEmitters.Add(gameObject.transform.Find("xBubbles_1").GetComponent<ParticleSystem>());

        _pistons = GetComponentsInChildren<PistonController>();

        _pistons[0].OnPressureReleased += () =>
        {
            _waitUntilPressureReleased = false;
            percentage = 0f;
            StartCoroutine(PlayFX());
        };
    }

    private IEnumerator PlayFX()
    {
        EmitterState(true);
        yield return new WaitForSeconds(1f);
        EmitterState(false);
    }

    private void EmitterState(bool state)
    {
        if (_isUnderWater)
        {
            foreach (ParticleSystem emitter in _bubblesParticleEmitters)
            {
                if (state)
                {
                    emitter.Play();
                }
                else
                {
                    emitter.Stop();
                }
            }
        }
        else
        {
            foreach (ParticleSystem emitter in _smokeParticleEmitters)
            {
                if (state)
                {
                    emitter.Play();
                }
                else
                {
                    emitter.Stop();
                }
            }
        }


    }

    // Update is called once per frame
    void Update()
    {
        //Build pressure
        if (_waitUntilPressureReleased) return;

        percentage += Time.deltaTime;

        if (percentage >= 100)
        {
            _waitUntilPressureReleased = true;
            SetPistonPercentage(100);
        }
        
        if (_pistons != null)
        {
            SetPistonPercentage(percentage/100);
        }
    }

    private void SetPistonPercentage(float percentage)
    {
        foreach (PistonController controller in _pistons)
        {
            controller.SetPercentage(percentage);
        }
    }
}
