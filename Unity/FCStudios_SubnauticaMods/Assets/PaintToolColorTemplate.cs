﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PaintToolColorTemplate : MonoBehaviour
{
    private Transform _grid;

    [SerializeField] 
    private PaintToolColorPickerEditor paintToolColorPickerEditor;

    private List<ColorTemplate> _defaultTemplates = new List<ColorTemplate>()
    {
        new ColorTemplate
        {
            PrimaryColor = Color.white,
            SecondaryColor = Color.gray,
            EmissionColor = Color.cyan
        },
        new ColorTemplate
        {
            PrimaryColor = Color.red,
            SecondaryColor = Color.gray,
            EmissionColor = Color.white
        },
        new ColorTemplate
        {
            PrimaryColor = Color.magenta,
            SecondaryColor = Color.gray,
            EmissionColor = Color.white
        },
    };

    private ToggleGroup _toggleGroup;
    private Action<ColorTemplate> _callBack;

    // Start is called before the first frame update
    void Start()
    {
        _grid = gameObject.transform.Find("Grid");
        for (int i = 0; i < _grid.childCount; i++)
        {
            if (i + 1 <= _defaultTemplates.Count)
            {
                var templateItem = _grid.GetChild(i).GetComponent<ColorPickerTemplateItemController>();
                templateItem.SetColors(_defaultTemplates[i]);
            }
        }

        _toggleGroup = gameObject.GetComponentInChildren<ToggleGroup>();

        var updateButton = gameObject.transform.Find("UpdateTemplateBTN").GetComponent<Button>();
        updateButton.onClick.AddListener((() =>
        {
            var selectedTemplate = GetSelectedTemplate();
            if (selectedTemplate != null)
            {
                paintToolColorPickerEditor.Open(selectedTemplate,this);
            }
        }));


        var cancelButton = gameObject.transform.Find("CancelBTN").GetComponent<Button>();
        cancelButton.onClick.AddListener((() =>
        {
            Close();
        }));

        var doneButton = gameObject.transform.Find("UseBTN").GetComponent<Button>();
        doneButton.onClick.AddListener((() =>
        {
            var selectedTemplate = GetSelectedTemplate();
            _callBack?.Invoke(selectedTemplate.GetTemplate());
            Close();
        }));
    }

    private void Close()
    {
        gameObject.SetActive(false);
    }

    private void Open(Action<ColorTemplate> callBack)
    {
        _callBack = callBack;
        gameObject.SetActive(true);
    }

    private ColorPickerTemplateItemController  GetSelectedTemplate()
    {
        return _toggleGroup.ActiveToggles().FirstOrDefault()?.gameObject.GetComponent<ColorPickerTemplateItemController>();
    }

    public class ColorTemplate
    {
        public Color PrimaryColor { get; set; } = Color.white;
        public Color SecondaryColor { get; set; } = Color.white;
        public Color EmissionColor { get; set; } = Color.white;
    }
}
