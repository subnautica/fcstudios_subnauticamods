﻿using UnityEngine;
using UnityEngine.UI;

public class ColorPickerTemplateItemController : MonoBehaviour
{
    private Image _primaryCircle;
    private Image _secondaryCircle;
    private Image _emissionCircle;

    private PaintToolColorTemplate.ColorTemplate _colorTemplate = new PaintToolColorTemplate.ColorTemplate();

    private void Awake()
    {
        _primaryCircle = gameObject.transform.Find("Primary").GetComponent<Image>();
        _secondaryCircle = gameObject.transform.Find("Secondary").GetComponent<Image>();
        _emissionCircle = gameObject.transform.Find("Emission").GetComponent<Image>();
    }

    public void SetColors(PaintToolColorTemplate.ColorTemplate template)
    {
        _colorTemplate = template;
        _primaryCircle.color = template.PrimaryColor;
        _secondaryCircle.color = template.SecondaryColor;
        _emissionCircle.color = template.EmissionColor;
    }

    public PaintToolColorTemplate.ColorTemplate GetTemplate()
    {
        return _colorTemplate;
    }
}
