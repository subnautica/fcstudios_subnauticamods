﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairsController : MonoBehaviour
{

    private void Start()
    {
        BuildGeometry(new Base(), false);
    }

	void BuildGeometry(Base baseComp, bool disableColliders)
	{
		if (this.stairsParent)
        {
            cachedSteps.Clear();
            foreach (Transform child in stairsParent.transform)
            {
                cachedSteps.Add(child.gameObject);
            }
        }
		this.stairsParent = new GameObject();
		this.stairsParent.transform.parent = base.transform;

		Vector3 a = this.traceStart.transform.up * -0.15f;
		float floorDistance = GetFloorDistance(a + this.traceStart.position - this.traceStart.right * 0.7f, this.traceStart.forward, maxLength, base.gameObject);
		float floorDistance2 = GetFloorDistance(a + this.traceStart.position + this.traceStart.right * 0.7f, this.traceStart.forward, maxLength, base.gameObject);
		float num;
		float num2;
		if (floorDistance > floorDistance2)
		{
			num = floorDistance;
			num2 = floorDistance2;
		}
		else
		{
			num = floorDistance2;
			num2 = floorDistance;
		}
		if (num > 0f)
		{
			this.BuildStairs(num, baseComp.isGhost, disableColliders);
			//this.constructableBounds.bounds.extents = new Vector3(1.4f, 0.01f, (num2 - 0.5f) / 2f);
			//this.constructableBounds.bounds.position = a + new Vector3(0f, 0.01f, (num2 - 0.5f) / 2f);
			return;
		}
		//this.constructableBounds.bounds.extents = Vector3.zero;
		//this.constructableBounds.bounds.position = Vector3.zero;
	}

	private GameObject CreateStep()
	{
		if (cachedSteps.Count > 0)
		{
			GameObject gameObject = cachedSteps[0];
			cachedSteps.Remove(gameObject);
			return gameObject;
		}
		return Instantiate(this.stepPrefab, this.stairsParent.transform);
	}

	private void BuildStairs(float length, bool isGhost, bool disableColliders)
	{
		int num = Mathf.RoundToInt(length / stepLength + 1.5f);
		//BaseName componentInParent = base.GetComponentInParent<BaseName>();
		for (int i = 0; i < num; i++)
		{
			GameObject gameObject = this.CreateStep();
			gameObject.transform.position = this.traceStart.position + (float)i * stepLength * this.traceStart.forward;
			gameObject.transform.rotation = base.transform.rotation;
			gameObject.SetActive(true);
			//if (!isGhost)
			//{
			//	ColorCustomizer component = gameObject.GetComponent<ColorCustomizer>();
			//	if (componentInParent.baseColors.Length != 0)
			//	{
			//		component.SetMainColor(componentInParent.baseColors[0]);
			//	}
			//	if (componentInParent.baseColors.Length > 1)
			//	{
			//		component.SetStripe1Color(componentInParent.baseColors[1]);
			//	}
			//	if (componentInParent.baseColors.Length > 2)
			//	{
			//		component.SetStripe2Color(componentInParent.baseColors[2]);
			//	}
			//}
		}
		Collider[] componentsInChildren = this.stairsParent.GetComponentsInChildren<Collider>();
		for (int j = 0; j < componentsInChildren.Length; j++)
		{
			componentsInChildren[j].enabled = !disableColliders;
		}
	}

	public Transform traceStart;

	public GameObject stepPrefab;

	//public ConstructableBounds constructableBounds;

    [Range(0, 1.671628f)]
    [SerializeField]
	private float stepLength = 1.671628f;

	private const float maxLength = 10f;

	private GameObject stairsParent;

	private static List<GameObject> cachedSteps = new List<GameObject>();

    private static bool IsDestroyed(GameObject gameObject)
    {
        return gameObject.tag == "ToDestroy";
    }

    private static bool IsOtherBasePiece(GameObject other, GameObject ignoreObject)
    {
        return (other.GetComponent<Base>() != null || other.GetComponentInParent<Base>() != null); // && !UWE.Utils.SharingHierarchy(other, ignoreObject);
    }
	public static bool IsBaseGhost(GameObject obj)
    {
        Transform transform = obj.transform;
        while (transform)
        {
            Base component = transform.GetComponent<Base>();
            if (component)
            {
                return component.isGhost;
            }
            transform = transform.parent;
        }
        return false;
    }

	public static float GetFloorDistance(Vector3 startPoint, Vector3 direction, float maxLength, GameObject ignoreObject = null)
    {
        float num = -1f;
        float num2 = -1f;
        bool flag = false;
        bool flag2 = false;
        int num3 = RaycastIntoSharedBuffer(startPoint, direction, maxLength, -1, QueryTriggerInteraction.Ignore);
        for (int i = 0; i < num3; i++)
        {
            RaycastHit raycastHit = sharedHitBuffer[i];
            if (/*raycastHit.collider.gameObject.layer != LayerID.Player &&*/ !IsBaseGhost(raycastHit.collider.gameObject) && !IsDestroyed(raycastHit.collider.gameObject))
            {
                if (IsOtherBasePiece(raycastHit.collider.gameObject, ignoreObject))
                {
                    if (num == -1f || num > raycastHit.distance)
                    {
                        num = raycastHit.distance;
                    }
                    flag = true;
                }
                else
                {
                    if (num2 == -1f || num2 > raycastHit.distance)
                    {
                        num2 = raycastHit.distance;
                    }
                    flag2 = true;
                }
            }
        }
        if (flag2 && flag && num2 - num > 0.3f)
        {
            return -1f;
        }
        return num2;
    }



	//DO NOT COPY

    public static RaycastHit[] sharedHitBuffer = new RaycastHit[256];

	public static int RaycastIntoSharedBuffer(Ray ray, float maxDistance = float.PositiveInfinity, int layerMask = -5, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        int num = Physics.RaycastNonAlloc(ray, sharedHitBuffer, maxDistance, layerMask, queryTriggerInteraction);
        if (GrowRaycastHitBufferIfNecessary(num))
        {
            return RaycastIntoSharedBuffer(ray, maxDistance, layerMask, queryTriggerInteraction);
        }
        return num;
    }

    public static int RaycastIntoSharedBuffer(Vector3 origin, Vector3 direction, float maxDistance = float.PositiveInfinity, int layerMask = -5, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        int num = Physics.RaycastNonAlloc(origin, direction, sharedHitBuffer, maxDistance, layerMask, queryTriggerInteraction);
        if (GrowRaycastHitBufferIfNecessary(num))
        {
            return RaycastIntoSharedBuffer(origin, direction, maxDistance, layerMask, queryTriggerInteraction);
        }
        return num;
    }

	private static bool GrowRaycastHitBufferIfNecessary(int numHits)
    {
        if (numHits >= sharedHitBuffer.Length)
        {
            sharedHitBuffer = new RaycastHit[sharedHitBuffer.Length * 2];
            return true;
        }
        return false;
    }
}
