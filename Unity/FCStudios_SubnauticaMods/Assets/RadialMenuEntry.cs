﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RadialMenuEntry : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler,IPointerClickHandler
{
    private Text _label;
    private string _buttonName;
    [SerializeField]
    private GameObject Hover;
    [SerializeField]
    private Image Icon;
    [SerializeField]
    private string HoverText;



    public void Initialize(Text pLabel,Sprite pIcon)
    {
        _label = pLabel;
        _buttonName = HoverText;
        Icon.sprite = pIcon;

    }

    private void SetLabel(string pText)
    {
        _label.text = pText;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        SetLabel(_buttonName);
        Hover.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        SetLabel(string.Empty); Hover.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log($"You clicked: {_buttonName}");
    }
}
