﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Makes objects float up & down while gently spinning.
public class Floater : MonoBehaviour
{
    // User Inputs
    public float amplitude = 0.5f;
    public float frequency = 1f;

    // Position Storage Variables
    private Vector3 originalLocalPosition;

    // Use this for initialization
    private void Start()
    {
        // Store the starting position & rotation of the object
        originalLocalPosition = transform.localPosition;
    }

    // Update is called once per frame
    private void Update()
    {
        Vector3 globalOffset = transform.up * Mathf.Sin(Time.time * Mathf.PI * frequency) * amplitude;

        Debug.Log($"Sin: {Mathf.Sin(Time.time * Mathf.PI * frequency)}");
        Debug.Log($"Tranform: {transform.up * Mathf.Sin(Time.time * Mathf.PI * frequency)}");
        Debug.Log($"Offset: {globalOffset}");

        transform.localPosition = originalLocalPosition;
        transform.position += globalOffset;
    }
}
