﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotorHandler : MonoBehaviour
{
    public float targetRotation;
    public TargetAxis targetAxis;
    public float speed = 2f;
    public bool useGlobal;
   
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        RotateRotor();
    }

    private void RotateRotor()
    {
        Quaternion currentRotation;
        if (useGlobal)
        {
            currentRotation = transform.rotation;
        }
        else
        {
            currentRotation = transform.localRotation;
        }
        
        Quaternion wantedRotation = default;
        switch (targetAxis)
        {
            case TargetAxis.X:
                wantedRotation = Quaternion.Euler(targetRotation, currentRotation.y, currentRotation.z);
                break;
            case TargetAxis.Y:
                wantedRotation = Quaternion.Euler(currentRotation.x, targetRotation, currentRotation.z);
                break;
            case TargetAxis.Z:
                wantedRotation = Quaternion.Euler(currentRotation.x, currentRotation.y, targetRotation);
                break;
        }

        if (useGlobal)
        {
            transform.rotation = Quaternion.RotateTowards(currentRotation, wantedRotation, Time.deltaTime * speed);
        }
        else
        {
            transform.localRotation = Quaternion.RotateTowards(currentRotation, wantedRotation, Time.deltaTime * speed);
        }
    }
}

public enum TargetAxis
{
    X, Y, Z
}
