﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class strinker : MonoBehaviour
{
    public Collider otherCollider;
    // Start is called before the first frame update
    void Start()
    {
        var colliders =  gameObject.GetComponentsInChildren<Collider>();
        var bounds = colliders[0].bounds;
        foreach (var c in colliders)
        {
            bounds.Encapsulate(c.bounds);
        }


        var szA = otherCollider.bounds.size;
        var szB = bounds.size;

        var targetX = szA.x / szB.x;
        var targetY = szA.y / szB.y;
        var targetZ = szA.z / szB.z;

        var minScale = Mathf.Min(new[] { targetX, targetY, targetZ });

        gameObject.transform.localScale = new Vector3(minScale, minScale, minScale);


        gameObject.transform.parent = otherCollider.transform;
        gameObject.transform.localPosition = Vector3.zero;
    }
}


