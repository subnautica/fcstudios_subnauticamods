﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Drone.Enum;
using UnityEngine;

public class DronePortController : MonoBehaviour//,IDroneDestination
{
    [SerializeField]
    private Transform entryPoint;
    [SerializeField]
    private Transform dockingPosition;
    private int _stateHash;
    private Animator _animator;
    private bool _isPlaying;
    public Transform BaseTransform { get; set; }
    public string BaseId { get; set; }
    private readonly List<PortDoorController> _doors = new List<PortDoorController>();

    [SerializeField]
    private List<GameObject> doors;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _stateHash = Animator.StringToHash("State");

        var door1 = doors.ElementAt(0).AddComponent<PortDoorController>();
        door1.OpenPosX = -1.25f;
        _doors.Add(door1);

        var door2 = doors.ElementAt(1).AddComponent<PortDoorController>();
        door2.OpenPosX = 1.25f;
        _doors.Add(door2);
    }

    public void Offload(Dictionary<string, int> order, Action onOffloadCompleted)
    {

    }

    public Transform GetDockingPosition()
    {
        return dockingPosition;
    }

    internal void OpenDoors()
    {
        foreach (PortDoorController door in _doors)
        {
            door.Open();
        }
    }

    internal void CloseDoors()
    {
        foreach (PortDoorController door in _doors)
        {
            door.Close();
        }
    }

    public Transform GetEntryPoint()
    {
        return entryPoint;
    }

    public IEnumerator PlayAndWaitForAnim(int valueToSet, string stateName, Action onCompleted)
    {
        _isPlaying = true;
        _animator.SetInteger(_stateHash, valueToSet);

        //Wait until we enter the current state
        while (!_animator.GetCurrentAnimatorStateInfo(0).IsName(stateName))
        {
            Debug.Log("Wait until we enter the current state.");
            yield return null;
        }

        //Now, Wait until the current state is done playing
        while ((_animator.GetCurrentAnimatorStateInfo(0).normalizedTime) % 1 < 0.99f)
        {
            Debug.Log("Now, Wait until the current state is done playing");
            yield return null;
        }

        //Done playing. Do something below!
        onCompleted?.Invoke();

        yield break;
    }

    public void PlayAnimationState(DronePortAnimation docking,Action callBack)
    {
        switch (docking)
        {
            case DronePortAnimation.None:
                PlayAnimation(0,"IDLE",callBack);
                CloseDoors();
                break;
            case DronePortAnimation.Docking:
                PlayAnimation(2, "Drone_Approaching", callBack);
                OpenDoors();
                break;
            case DronePortAnimation.Departing:
                PlayAnimation(1, "Drone_Departing", callBack);
                OpenDoors();
                break;
            default:
                PlayAnimation(0, "IDLE", callBack);
                CloseDoors();
                break;
        }
    }

    private void PlayAnimation(int value,string animationName,Action callBack)
    {
        if (_isPlaying) return;

        StartCoroutine(PlayAndWaitForAnim(value, animationName, () =>
            {
                _isPlaying = false;
                callBack?.Invoke();
                CloseDoors();
              _animator.SetInteger(_stateHash, 0);
            }
        ));
    }
}
