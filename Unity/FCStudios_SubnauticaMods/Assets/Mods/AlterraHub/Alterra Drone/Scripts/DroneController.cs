﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Drone;
using Assets.Scripts.Drone.Enum;
using Assets.Scripts.Drone.States;
using UnityEngine;

public class DroneController : MonoBehaviour
{
    private Dictionary<string, int> _order;

    [SerializeField]
    private DronePortController departurePort;

    [SerializeField]
    private DronePortController destinationPort;

    private Transform _trans;
    private StateMachine _stateMachine;
    public StateMachine StateMachine => GetStateMachine();

    private StateMachine GetStateMachine()
    {
        if (_stateMachine == null)
        {
            _stateMachine =  GetComponent<StateMachine>();
        }

        return _stateMachine;
    }

    public Transform Destination { get; set; }


    private void Awake()
    {
        InitializeStateMachine();
        Destination = destinationPort?.GetEntryPoint();
        _trans = gameObject.transform;

    }
    
    

    private void InitializeStateMachine()
    {
        var states = new Dictionary<Type, BaseState>()
        {
            {typeof(IdleState), new IdleState(this)},
            {typeof(TransportState), new TransportState(this)},
            {typeof(DockingState), new DockingState(this)},
            {typeof(DepartState), new DepartState(this)},
            {typeof(AlignState), new AlignState(this)},
        };

        GetComponent<StateMachine>().SetStates(states);
    }

    public void TryDepart()
    {
        Depart();
        ShipOrder(new Dictionary<string, int>{ {"Copper", 5}}, null, null);
        GetComponent<StateMachine>().SwitchToNewState(typeof(DepartState));
    }
    
    internal void ShipOrder(Dictionary<string, int> order, GameObject departurePort, GameObject destinationPort)
    {
        Debug.Log("Trying to ship");
        _order = order;
     }

    public void Dock()
    {
        destinationPort.PlayAnimationState(DronePortAnimation.Docking, () =>
        {
            GetComponent<StateMachine>().SwitchToNewState(typeof(IdleState));
        });
    }
    
    public void Depart()
    {
        departurePort.PlayAnimationState(DronePortAnimation.Departing,(() =>
        {
            GetComponent<StateMachine>().SwitchToNewState(typeof(TransportState));
        }));
    }

    public Transform GetTransform()
    {
        return _trans;
    }

    public DronePortController GetTargetPort()
    {
        return destinationPort;
    }

    public DronePortController GetCurrentPort()
    {
        return departurePort;
    }
}
