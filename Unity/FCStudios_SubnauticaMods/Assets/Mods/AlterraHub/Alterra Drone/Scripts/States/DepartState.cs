﻿using System;
using UnityEngine;

namespace Assets.Scripts.Drone.States
{
    public class DepartState : BaseState
    {
        private readonly DroneController _drone;

        public DepartState(DroneController drone) : base(drone.gameObject)
        {
            _drone = drone;
        }

        public override Type Tick()
        {
            Debug.Log("Departing");
            _drone.Depart();
            var followPoint = _drone.GetCurrentPort().GetEntryPoint();
            transform.position = followPoint.position;
            transform.rotation = followPoint.rotation;
            return null;
        }
    }
}
