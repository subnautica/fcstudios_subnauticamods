﻿using System;
using UnityEngine;

namespace Assets.Scripts.Drone.States
{
    public class DockingState : BaseState
    {
        private readonly DroneController _drone;

        public DockingState(DroneController drone) : base(drone.gameObject)
        {
            _drone = drone;
        }

        public override Type Tick()
        {
            Debug.Log("Docking");
            _drone.Dock();
            var followPoint = _drone.GetTargetPort().GetEntryPoint();
            transform.position = followPoint.position;
            transform.rotation = followPoint.rotation;
           
            return null;
        }
    }
}
