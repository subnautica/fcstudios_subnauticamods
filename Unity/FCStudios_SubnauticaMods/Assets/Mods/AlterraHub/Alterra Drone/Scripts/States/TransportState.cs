﻿using System;
using UnityEngine;

namespace Assets.Scripts.Drone.States
{
    public class TransportState : BaseState
    {
        private readonly DroneController _drone;
        private const float speed = 5f;
        public TransportState(DroneController drone) : base(drone.gameObject)
        {
            _drone = drone;
        }

        public override Type Tick()
        {

            if (_drone.Destination == null)
            {
                return typeof(IdleState);
            }

            if (transform.position == _drone.Destination.position)
            {
                return typeof(AlignState);
            }
            
            transform.position = Vector3.MoveTowards(transform.position, _drone.Destination.position, speed * Time.deltaTime);
            var rotation = Quaternion.LookRotation(_drone.Destination.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 1 * Time.deltaTime);

            Debug.Log("Transporting");
            return null;
        }
    }
}
