﻿using System;
using UnityEngine;

namespace Assets.Scripts.Drone.States
{
    public class IdleState : BaseState
    {
        public IdleState(DroneController drone) : base(drone.gameObject)
        {
        }

        public override Type Tick()
        {
            Debug.Log("Idle");
            return null;
        }
    }
}
