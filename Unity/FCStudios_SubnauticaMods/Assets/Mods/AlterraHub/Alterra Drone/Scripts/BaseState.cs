﻿using System;
using UnityEngine;

namespace Assets.Scripts.Drone
{
    public abstract class BaseState
    {
        protected BaseState(GameObject gameObject)
        {
            this.gameObject = gameObject;
            this.transform = gameObject.transform;
        }

        protected GameObject gameObject;
        protected Transform transform;
        public abstract Type Tick();
    }
}
