﻿namespace Assets.Scripts.Drone.Enum
{
    public enum DronePortAnimation
    {
        None = 0,
        Docking,
        Departing
    }
}
