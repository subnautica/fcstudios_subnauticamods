﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface IDroneDestination
{
    Transform BaseTransform { get; set; }
    string BaseId { get; set; }
    Transform GetEntryPoint();
    void Offload(Dictionary<string, int> order, Action onOffloadCompleted);
    Transform GetDockingPosition();
    void OpenDoors();
    void CloseDoors();
}
