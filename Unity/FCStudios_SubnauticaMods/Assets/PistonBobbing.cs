﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistonBobbing : MonoBehaviour
{
    // User Inputs
    public float amplitude = -0.1f;//0.5f;
    public float frequency = 3.39f;//1f;

    // Position Storage Variable
    private Vector3 originalLocalPosition;
    private Vector3 originalPosition;
    [SerializeField]
    private bool _isRunning;
    [SerializeField]
    private float _offset;

    private float _timingOffset;
    public bool Invert { get; set; }

    // Use this for initialization
    private void Start()
    {
        // Store the starting position of the object relative to its parent.
        originalLocalPosition = transform.localPosition;
        originalPosition = transform.position;
    }

    private void Awake()
    {
        _timingOffset = Random.value * (Mathf.PI / 2);
    }

    public void SetState(bool isRunning)
    {
        _isRunning = isRunning;
    }

    // Update is called once per frame  
    private void Update()
    {
        if (!_isRunning) return;
        // Calculate offset
        // Even though Transform.up is relative, it is in world space
        // That means that you can't apply it to localPosition
        //
        // Vector3.up could be used along with localPosition, but that would ignore the object's rotation

        Vector3 globalOffset = transform.up * Mathf.Sin((Time.time + _timingOffset) * Mathf.PI * frequency) * amplitude;


        if (Vector3.Distance(transform.position + globalOffset, originalPosition) > 0f)
        {
            transform.position += globalOffset;
        }
        // Reset the position to the original
        transform.localPosition = originalLocalPosition;
        var distance = Vector3.Distance(transform.position + globalOffset, originalPosition);
        
        Debug.Log(distance);

        if (distance > 0f)
        {
            //transform.position += globalOffset;
        }
    }
}
