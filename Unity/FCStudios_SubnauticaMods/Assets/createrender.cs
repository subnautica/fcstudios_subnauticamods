﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class createrender : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var rawImage = gameObject.GetComponentInChildren<RawImage>();
        var video = gameObject.GetComponentInChildren<VideoPlayer>();
        
        var renderTexture = new RenderTexture(1920, 1080, 24);
        renderTexture.Create();
        video.targetTexture = renderTexture;
        rawImage.texture = renderTexture;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
